const randomstring = require("randomstring");
const jsonfile = require ("jsonfile");


const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}
var contents = jsonfile.readFileSync(inputFile);

console.log("lenght of an array is :", contents.names.length + " and display the original array"); // display lenght of an array 
console.log(contents.names); //read and display the json file
var access = contents.names; //contents of json stored in access variable

//function to reverse an array 

function reverseArray(arr) { // using  reverseArray function and map method  to reverse the each string in a json array
 return arr.map (item => item.split('').reverse().join(''));

}
console.log("Reversed each string", reverseArray(access));


console.log("generating 20 pieces of 5 character nonsense and add @gmail.com");

output.emails = [];
for (var i=0; i<20 ; i++){
    output.emails.push(reverseArray(access)[i] + randomstring.generate(5) + "@gmail.com");
}
console.log("generated fakemail", output.emails);


jsonfile.writeFile(outputFile,  output, {spaces: 2} , function(err) {  //write output to outputFile

    console.log("All done!"); 

}); 